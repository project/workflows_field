<?php

declare(strict_types = 1);

namespace Drupal\workflows_field\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the workflows field.
 */
class WorkflowsFieldContraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Creates an instance of WorkflowsFieldContraintValidator.
   */
  final private function __construct(
    private EntityTypeManagerInterface $entityTypeManager,
    private AccountInterface $currentUser,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param \Drupal\workflows_field\Plugin\Field\FieldType\WorkflowsFieldItem $value
   * @phpstan-param \Drupal\workflows_field\Plugin\Validation\Constraint\WorkflowsFieldContraint $constraint
   */
  public function validate($value, Constraint $constraint): void {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $value->getEntity();
    $workflow_type = $value->getWorkflow()->getTypePlugin();

    // An entity can start its life in any state.
    if (!isset($value->value) || $entity->isNew()) {
      return;
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $original_entity */
    $original_entity = $this->entityTypeManager->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
    if (!$entity->isDefaultTranslation() && $original_entity->hasTranslation($entity->language()->getId())) {
      $original_entity = $original_entity->getTranslation($entity->language()->getId());
    }
    $previous_state = $original_entity->{$value->getFieldDefinition()->getName()}->value;

    // The state does not have to change.
    if ($previous_state === $value->value) {
      return;
    }

    if (!$workflow_type->hasTransitionFromStateToState($previous_state, $value->value)) {
      $this->context->addViolation($constraint->message, [
        '%state' => $value->value,
        '%previous_state' => $previous_state,
      ]);
    }
    else {
      $transition = $workflow_type->getTransitionFromStateToState($previous_state, $value->value);
      if (!$this->currentUser->hasPermission(sprintf('use %s transition %s', $value->getWorkflow()->id(), $transition->id()))) {
        $this->context->addViolation($constraint->insufficientPermissionsTransition, [
          '%transition' => $transition->label(),
        ]);
      }
    }
  }

}
