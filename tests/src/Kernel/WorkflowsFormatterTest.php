<?php

declare(strict_types = 1);

namespace Drupal\Tests\workflows_field\Kernel;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\node\Entity\Node;

/**
 * Tests the Workflows Field formatters.
 *
 * @group workflows_field
 */
class WorkflowsFormatterTest extends WorkflowsTestBase {

  /**
   * Test the states list formatter.
   */
  public function testStatesListFormatter(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'project',
      'field_status' => 'in_discussion',
    ]);
    $node->save();

    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldStatusList */
    $fieldStatusList = $node->get('field_status');

    $output = $fieldStatusList->view(['type' => 'workflows_field_state_list']);
    $this->assertEquals([
      [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'Implementing',
        '#wrapper_attributes' => ['class' => ['implementing', 'before-current']],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'Approved',
        '#wrapper_attributes' => ['class' => ['approved', 'before-current']],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'Rejected',
        '#wrapper_attributes' => ['class' => ['rejected', 'before-current']],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'Planning',
        '#wrapper_attributes' => ['class' => ['planning', 'before-current']],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'In Discussion',
        '#wrapper_attributes' => ['class' => ['in_discussion', 'is-current']],
      ],
    ], $output[0]['#items']);

    // Try with settings excluded.
    $output = $fieldStatusList->view([
      'type' => 'workflows_field_state_list',
      'settings' => [
        'excluded_states' => [
          'in_discussion' => 'in_discussion',
          'planning' => 'planning',
          'rejected' => 'rejected',
          'approved' => 'approved',
        ],
      ],
    ]);
    $this->assertEquals([
      [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'Implementing',
        '#wrapper_attributes' => ['class' => ['implementing', 'before-current']],
      ],
    ], $output[0]['#items']);
  }

  /**
   * Test the default formatter.
   */
  public function testDefaultFormatter(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'project',
      'field_status' => 'in_discussion',
    ]);
    $node->save();

    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldStatusList */
    $fieldStatusList = $node->get('field_status');

    $this->assertEquals([
      '#markup' => 'In Discussion',
      '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
    ], $fieldStatusList->view()[0]);
  }

}
