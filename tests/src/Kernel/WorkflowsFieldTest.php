<?php

declare(strict_types = 1);

namespace Drupal\Tests\workflows_field\Kernel;

use Drupal\node\Entity\Node;
use Drupal\workflows\Entity\Workflow;
use Drupal\workflows_field\Plugin\Field\FieldType\WorkflowsFieldItem;

/**
 * Test the workflows field.
 *
 * @group workflows_field
 */
class WorkflowsFieldTest extends WorkflowsTestBase {

  /**
   * Test the implementation of OptionsProviderInterface.
   */
  public function testOptionsProvider(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'project',
      'field_status' => 'in_discussion',
    ]);
    $node->save();

    /** @var \Drupal\workflows_field\Plugin\Field\FieldType\WorkflowsFieldItem $fieldStatusItem */
    $fieldStatusItem = $node->get('field_status')->first();

    $this->assertEquals([
      'implementing' => 'Implementing',
      'approved' => 'Approved',
      'rejected' => 'Rejected',
      'planning' => 'Planning',
      'in_discussion' => 'In Discussion',
    ], $fieldStatusItem->getPossibleOptions());
    $this->assertEquals([
      'approved' => 'Approved',
      'rejected' => 'Rejected',
      'in_discussion' => 'In Discussion',
    ], $fieldStatusItem->getSettableOptions());

    $this->assertEquals([
      'implementing',
      'approved',
      'rejected',
      'planning',
      'in_discussion',
    ], $fieldStatusItem->getPossibleValues());
    $this->assertEquals([
      'approved',
      'rejected',
      'in_discussion',
    ], $fieldStatusItem->getSettableValues());
  }

  /**
   * Settable options are filtered by the users permissions.
   */
  public function testOptionsProviderFilteredByUser(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'project',
      'field_status' => 'in_discussion',
    ]);
    $node->save();

    /** @var \Drupal\workflows_field\Plugin\Field\FieldType\WorkflowsFieldItem $fieldStatusItem */
    $fieldStatusItem = $node->get('field_status')->first();

    // If a user has no permissions then the only available state is the current
    // state.
    $this->assertEquals([
      'in_discussion' => 'In Discussion',
    ], $fieldStatusItem->getSettableOptions($this->createUser()));

    // Grant the ability to use the approved_project transition and the user
    // should now be able to set the Approved state.
    $this->assertEquals([
      'in_discussion' => 'In Discussion',
      'approved' => 'Approved',
    ], $fieldStatusItem->getSettableOptions($this->createUser(['use bureaucracy_workflow transition approved_project'])));
  }

  /**
   * @covers \Drupal\workflows_field\Plugin\Field\FieldType\WorkflowsFieldItem
   */
  public function testFieldType(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'project',
      'field_status' => 'in_discussion',
    ]);
    $node->save();

    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldStatusList */
    $fieldStatusList = $node->get('field_status');
    /** @var \Drupal\workflows_field\Plugin\Field\FieldType\WorkflowsFieldItem $fieldStatusItem */
    $fieldStatusItem = $fieldStatusList->first();

    // Test the dependencies calculation.
    $this->assertEquals([
      'config' => [
        'workflows.workflow.bureaucracy_workflow',
      ],
    ], WorkflowsFieldItem::calculateStorageDependencies($fieldStatusList->getFieldDefinition()->getFieldStorageDefinition()));

    // Test the getWorkflow method.
    $this->assertEquals('bureaucracy_workflow', $fieldStatusItem->getWorkflow()->id());
  }

  /**
   * @covers \Drupal\workflows_field\Plugin\WorkflowType\WorkflowsField
   */
  public function testWorkflowType(): void {
    // Test the initial state based on the config, despite the state weights.
    /** @var \Drupal\workflows_field\Plugin\WorkflowType\WorkflowsField $type */
    $type = Workflow::load('bureaucracy_workflow')->getTypePlugin();
    $this->assertEquals('in_discussion', $type->getInitialState()->id());
  }

}
